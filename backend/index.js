const express = require('express')
const cors = require('cors')
const path = require('path')
const logger = require('./src/logger')

const router = require('./src/routes/apis')

const app = express()

app.use(express.json())
app.use(express.urlencoded({extended:false}))

app.use(cors())

//routes

app.use('/admins', router)

app.use((request,response,next) => {
    const error = new Error("NOT FOUND")
    next(error)
})

app.use((error,request,response,next) => {
    response.status(404 || 500).json({message: error.message})
});


//static files
app.use(express.static(path.join(__dirname,'../frontend/build')))

app.get("/*", (request,response) => {
    response.sendFile(path.join(__dirname,'../frontend/build/index.html'),(error) => {
        if (error) {
            response.status(500).json({responseMessage: error})
        }
    })
})

const PORT = process.env.PORT || 5000

app.listen(PORT,()=>logger.info(`Server started listening on PORT : ${PORT}`));

module.exports = app;