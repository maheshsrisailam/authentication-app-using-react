import { Link, useNavigate } from 'react-router-dom'
import Cookies from 'js-cookie'
import { useEffect, useState } from 'react'
import './index.css'
import { isValidEmail, isValidPassword } from '../../validations/inputValidations'

const SignIn = (props) => {

    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [errorMessage, setErrorMessage] = useState("")
    const [loginStatus, setLoginStatus] = useState(false)

    console.log(loginStatus)
    const navigate = useNavigate()

    const loginSuccess = (jwtToken) => {
        const inFifteenMinutes = new Date(new Date().getTime() + 2 * 60 * 1000);
        Cookies.set('JWT_TOKEN',jwtToken, {expires: inFifteenMinutes})
        navigate('/')
    }

    const loginFails = (errorMessage) => {
        setErrorMessage(errorMessage)
    }

    useEffect(() => {
        if (loginStatus) {
            if (!isValidEmail(email)) {
                setErrorMessage("Invalid Email")
            } else if (!isValidPassword(password)) {
                setErrorMessage("Characters in the password lies between 5 to 256")
            } else {
                const loginDetails = {
                    email,
                    password
                }
                const loginApi = async () => {
                    const apiUrl = "http://localhost:5000/admins/login"
                    const options = {
                        method: "POST",
                        headers: {
                            "Content-Type": 'application/json',
                            "Accept": "application/json"
                        },
                        body: JSON.stringify(loginDetails)
                    }
                    const response = await fetch(apiUrl,options)
                    const data = await response.json()
                    console.log(response)
                    if (response.ok) {
                        loginSuccess(data.JWT_TOKEN)
                    } else if (response.status === 401) {
                        setErrorMessage(`Status code: ${response.status} : User with EMAIL: ${email} is ${response.statusText}`)
                    } else if (response.status === 404) {
                        setErrorMessage(`Status code: ${response.status} : User with EMAIL: ${email} is ${response.statusText}`)
                    } else {
                        loginFails(data.error_msg)
                    }
                }
                loginApi()   
            }
            
        }
    },[loginStatus,email,password])

    const handleOnSubmit = (event) => {
        event.preventDefault()
        setLoginStatus(prevLoginStatus=>!prevLoginStatus)
    }

    const renderHeadingContainer = () => (
        <div className="login-page-heading-container">
            <h1 className="login-page-main-heading">Welcome Back!</h1>
            <p className="login-page-description">For login to your account, please fill all the required fields in the form.</p>
        </div>
    )

    const renderEmailSection = () => (
        <>
            <label htmlFor="email" className="login-page-label-elements">EMAIL</label>
            <br />
            <input type="text" className="login-page-input-elements" value={email} onChange={event=>setEmail(event.target.value)} id="email" placeholder="Enter your email" required />
            <br />
        </>
    )

    const renderPasswordSection = () => (
        <>
            <label htmlFor="password" className="login-page-label-elements">PASSWORD</label>
            <br />
            <input type="password" className="login-page-input-elements" value={password} onChange={event=>setPassword(event.target.value)} id="password" placeholder="Enter your password" required />
            <br />
        </>
    )

    const renderRememberMeForgotSection = () => (
        <div className="remember-me-forgot-password-container">
                <div className="checkbox-remember-container">
                    <input type="checkbox" id="checkbox" className="checkbox" />
                    <label htmlFor='checkbox' className='checkbox-label'>Remember Me</label>
                </div>
                <p className='forgot-password'>Forgot Password ?</p>
        </div>
    )

    const renderButtonsSection = () => (
        <>
            <button type="submit" className="login">LOG IN</button>
            <br />
            <Link to="/signup" className='link-style'>
                <button type="button" className="signup">SIGN UP</button>
            </Link>
        </>
    )

    const renderFormSection = () => (
        <form onSubmit={handleOnSubmit} className="login-page-form-container">
            {renderEmailSection()}
            {renderPasswordSection()}
            {renderRememberMeForgotSection()}
            {errorMessage && <p className='error-message'>{errorMessage}</p>}
            {renderButtonsSection()}
        </form>
    )


    return ( 
        <div className="login-page-main-container">
            <div className='login-page-card'>
                {renderHeadingContainer()}
                {renderFormSection()}
            </div>
        </div>
     );
}
 
export default SignIn;