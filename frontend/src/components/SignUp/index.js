import {Link,useNavigate} from 'react-router-dom'
import { useEffect, useState } from "react"
import './index.css'
import { isValidEmail, isValidName, isValidPassword } from '../../validations/inputValidations'

const SignUp = (props) => {
    const [firstname, setFirstname] = useState("")
    const [lastname, setLastname] = useState("")
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [errorMessage,setErrorMessage] = useState("")
    const [isSignUpClicked,setSignUpClicked] = useState(false)

    const navigate = useNavigate()
    
    function onSuccessSignUp() {
        navigate('/login')
        setFirstname("")
        setLastname("")
        setEmail("")
        setPassword("")
    }

    function onFailureSignUp(errorMessage) {
        setErrorMessage(errorMessage)
    }

    useEffect(() => {
        if (isSignUpClicked) {
           if (!isValidName(firstname)) {
                setErrorMessage(`Firstname: ${firstname}. It contains only alphabets and characters count lies between 3 to 30 letters`)
           } else if (!isValidName(lastname)) {
            setErrorMessage(`Lastname: ${lastname}. It contains only alphabets and characters count lies between 3 to 30 letters`)
           } else if (!isValidEmail(email)) {
                setErrorMessage(`Email: ${email} is not a valid email.`)
           } else if (!isValidPassword(password)) {
                setErrorMessage('Characters count in the password lies between 5 to 256.')
           } else {
            const signupApiUrl = "http://localhost:5000/admins/signup" 
            const userData = {firstname,lastname,email,password}
            const signupApi = async () => {
                const options = {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                        "Accept": "application/json"
                    },
                    body: JSON.stringify(userData)
                }
                const response = await fetch(signupApiUrl,options)
                const data = await response.json()
                console.log(data)
                console.log(response)
                if (response.ok) {
                    onSuccessSignUp()
                } else if (response.status === 403) {
                    setErrorMessage(`Status code: ${response.status} : User with EMAIL: ${email} is ${response.statusText}`)
                } else {
                    onFailureSignUp(data.error_msg)
                }
            }
            signupApi()
           }
        }
    },[isSignUpClicked])

    const handleOnSubmit = (event) => {
        event.preventDefault()
        setSignUpClicked(prevState=>!prevState)
    }

    const renderHeadingContainer = () => (
        <div className="heading-container">
            <h1 className="main-heading">Create your Account!</h1>
            <p className="description">For creating your account, please fill all the required fields in the form.</p>
        </div>
    )

    const renderFirstnameLastnameContainer = () => (
        <div className="firstname-lastname-container">
            <div className="name-label-elements">
                <label htmlFor="firstname" className="label-elements">FIRST NAME</label>
                <br />
                <input type="text" className="input-elements" value={firstname} onChange={(event)=> setFirstname(event.target.value)} placeholder="Enter your first name" id="firstname" required />
            </div>
            <div className="name-label-elements">
                <label htmlFor="lastname" className="label-elements">LAST NAME</label>
                <br />
                <input type="text" className="input-elements" value={lastname} onChange={(event)=> setLastname(event.target.value)} placeholder="Enter your last name" id="lastname" required />
            </div>
        </div>
    )

    const renderEmailSection = () => (
        <>
            <label htmlFor="email" className="label-elements">EMAIL</label>
            <br />
            <input type="text" className="input-elements" value={email} onChange={event=>setEmail(event.target.value)} id="email" placeholder="Enter your email" required />
            <br />
        </>
    )

    const renderPasswordSection = () => (
        <>
            <label htmlFor="password" className="label-elements">PASSWORD</label>
            <br />
            <input type="password" className="input-elements" value={password} onChange={event=>setPassword(event.target.value)} id="password" placeholder="Enter your password" required />
            <br />
        </>
    )

    const renderButtonsSection = () => (
        <>
            <button type="submit" className="signup-button">SIGN UP</button>
            <br />
            <Link to="/login" className='link-style'>
            <button type="button" className="login-button">LOG IN</button>
            </Link>
        </>
    )

    const renderFormSection = () => (
        <form onSubmit={handleOnSubmit} className="form-container">
            {renderFirstnameLastnameContainer()}
            {renderEmailSection()}
            {renderPasswordSection()}
            {errorMessage && <p className='error-message'>{errorMessage}</p>}
            {renderButtonsSection()}
        </form>
    )
   

    return (
        <div className="sign-up-main-container">
           <div className="card">
                {renderHeadingContainer()}
                {renderFormSection()}
           </div>
        </div>
    )
}

export default SignUp