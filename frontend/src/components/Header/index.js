import {Link,useNavigate} from 'react-router-dom'
import Cookies from 'js-cookie'
import './index.css'

const Header = (props) => {
    const navigate = useNavigate()
    const handleLogout = () => {
        Cookies.remove("JWT_TOKEN")
        navigate('/login')
    }
    return ( 
        <div className="header-container">
            <h1 className="header-title">Authentication System</h1>
            <Link to="/login">
                <button className="logout-button" onClick={handleLogout}>LOGOUT</button>
            </Link>
        </div>
     );
}
 
export default Header;