import {Link} from 'react-router-dom'
import './index.css'
const NotFound = () => {
    return (
        <div className="not-found-container">
            <img src="https://www.pngitem.com/pimgs/m/254-2549834_404-page-not-found-404-not-found-png.png" className="not-found-image" alt="not-found" />
            <Link to="/login">
                <button className='go-to-home-button'>
                    Go to Home
                </button>
            </Link>
        </div>  
    )
}

export default NotFound;