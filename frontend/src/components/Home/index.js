import Cookies from 'js-cookie';
import { useEffect, useState } from 'react';
import { Navigate } from 'react-router-dom';
import Header from '../Header';
import './index.css'

const Home = () => {
    const jwtToken = Cookies.get("JWT_TOKEN")

    const [adminsArray, setData] = useState([])

    useEffect(() => {
        const getData = async () => {
            const options = {
                method: "GET",
                headers: {
                    Authorization : `Bearer ${jwtToken}` 
                } 
            }
            const response = await fetch("http://localhost:5000/admins/admins",options)
    
            const data = await response.json()
            setData(data)
            console.log(data)
        }
        getData()
    },[])

    const renderHomeView = () => (
        <div> 
            <Header />
            <div className="home-page-main-container">
                <h1 className='home-page-heading'>Users</h1>
                <ol className='lists-container'>
                    {adminsArray.map((user)=><li key={user.id} className="list-item">{user.email}</li>)}
                </ol>
            </div>
        </div>
    )

    return ( 
        <>
            {jwtToken === undefined ? <Navigate to='/login' /> : renderHomeView()}
        </>
     );
}
 
export default Home;