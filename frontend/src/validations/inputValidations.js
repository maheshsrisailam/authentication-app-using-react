export const isValidName = (name) => {
    const regex = /^[a-zA-Z ]{3,30}$/;
    return regex.test(name)
}

export const isValidEmail = (email) => {
    const regex = /^[a-z0-9]+@[a-z]+\.[a-z]{2,3}$/;
    return regex.test(email)
}

export const isValidMObileNumber = (mobilenumber) => {
    const regex = /^\d{10}$/;
    return regex.test(mobilenumber)
}

export const isValidPassword = (password) => {
    return  ((password.length >= 5) && (password.length <= 256)) 
}
