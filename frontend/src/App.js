import {BrowserRouter,Route,Routes,Navigate} from 'react-router-dom'
import SignUp from './components/SignUp/index'
import SignIn from './components/SignIn/index'
import Home from './components/Home/index'
import NotFound from './components/NotFound'


const App = () => {
  return (
      <BrowserRouter>
        <Routes>
          <Route path='/signup' element={<SignUp />} />
          <Route path="/login" element={<SignIn />} />
          <Route exact path='/' element={<Home />} />
          <Route path='/not-found' element={<NotFound />} />
          <Route path='*' element={<NotFound />} />
        </Routes>
    </BrowserRouter>
   );
}
 
export default App;
